package afu;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Maidenhead {

	private Maidenhead() {}

    private static final int offset = (int) 'A';

    public static LatLong toRadians(String loc) {
    //  Input:  	Maidenhead locator 
    //  Output: 	latitude, longitude [rad]
        String locator = loc.toUpperCase();
        double longitude = 0.0;
        double latitude = 0.0;

        Pattern p = Pattern.compile("^(\\w)(\\w)");
        Matcher m = p.matcher(locator);
        if (m.find()) {
            longitude = m.group(1).charAt(0);
            latitude = m.group(2).charAt(0);
            longitude = -180.0 + (longitude - offset) * 20.0;
        }
        p = Pattern.compile("^\\w\\w(\\d)");
        m = p.matcher(locator);
        if (m.find()) {
            longitude += Double.parseDouble(m.group(1)) * 2.0;
        }
        p = Pattern.compile("^\\w\\w\\d\\d(\\w)");
        m = p.matcher(locator);

    //  mid of the subsquare or of the grid square (incomplete statement: JO31)
        if (m.find()) {
            longitude += (((m.group(1).charAt(0) - offset) * 5.0) + 2.5) / 60.0;
        } else {
            longitude += 1.0;
        }
        latitude = -90.0 + (latitude - offset) * 10.0;
        p = Pattern.compile("^\\w\\w\\d(\\d)");
        m = p.matcher(locator);
        if (m.find()) {
            latitude += Double.parseDouble(m.group(1));
        }
        p = Pattern.compile("^\\w\\w\\d\\d\\w(\\w)");
        m = p.matcher(locator);

    //  mid of the subsquare or of the grid square (incomplete statement: JO31)
        if (m.find()) {
            latitude += (((m.group(1).charAt(0) - offset) * 2.5) + 1.25) / 60.0;
        } else {
            latitude += 0.5;
        }
        latitude = Math.toRadians(latitude);
        longitude = Math.toRadians(longitude);

        return new LatLong(latitude, longitude); // [rad]
    }
}
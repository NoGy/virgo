package afu;

public class SkyDisplay {

	private final Virgo virgo;

    public SkyDisplay(Virgo virgo) {
		this.virgo = virgo;
    }
    
    public void mkRadiants() {
        String date = DateTimeUTC.getDateUTC();
        String time = DateTimeUTC.getTimeUTC();
        for (Shower s: this.virgo.showers) {
             AzimuthElevation ae = AstroCalculation.getAzimuthElevation(date, time, this.virgo.myLatLong.latitude, this.virgo.myLatLong.longitude, Math.toRadians(s.ra), Math.toRadians(s.decl));
             double azimuth = ae.azimuth;
             if (!this.virgo.we) {
                 azimuth *= -1;           // E on the left side!
             }
             XY xy = AstroCalculation.toCartesian(azimuth, ae.elevation, this.virgo.f_sd_axis / 2, this.virgo.sd_x_root, this.virgo.sd_y_root);
             this.virgo.radiants.add(this.virgo.new sdScreenCoordinates(s.ref, s.mclass, xy.x, xy.y));
        } 
    }
    
    public void mkoswinRadiants() {
        String date = DateTimeUTC.getDateUTC();
        String time = DateTimeUTC.getTimeUTC();
        for (Shower s: this.virgo.showers) {
             AzimuthElevation ae = AstroCalculation.getAzimuthElevation(date, time, this.virgo.oswinLatLong.latitude, this.virgo.oswinLatLong.longitude, Math.toRadians(s.ra), Math.toRadians(s.decl));
             double azimuth = ae.azimuth;
             if (!this.virgo.we) {
                 azimuth *= -1;           // E on the left side!
             }
             XY xy = AstroCalculation.toCartesian(azimuth, ae.elevation, 40, this.virgo.os_x_root, this.virgo.os_y_root);
             this.virgo.oswin_radiants.add(this.virgo.new sdScreenCoordinates(s.ref, s.mclass, xy.x+2, xy.y));
        } 
    }
    
    public void mkMaxima() {
        String date = DateTimeUTC.getDateUTC();
        for (Shower s: this.virgo.showers) {
            if (date.equals(s.peak_date) && s.peak_time != "") {
                 AzimuthElevation ae = AstroCalculation.getAzimuthElevation(s.peak_date, s.peak_time + ":00", this.virgo.myLatLong.latitude, this.virgo.myLatLong.longitude, Math.toRadians(s.ra), Math.toRadians(s.decl));
                 double azimuth = ae.azimuth;
                 if (!this.virgo.we) {
                    azimuth *= -1;           // E on the left side!
                }
                 XY xy = AstroCalculation.toCartesian(azimuth, ae.elevation, this.virgo.f_sd_axis / 2, this.virgo.sd_x_root, this.virgo.sd_y_root);
                 this.virgo.maxima.add(this.virgo.new sdScreenCoordinates(s.ref, 1000, xy.x, xy.y));
             }
        } 
    }
    
    public void mkTicks() {
        String date = DateTimeUTC.getDateUTC();
        for (Shower s: this.virgo.showers) {
            for(Integer h = 0; h <= 23; h++) {
               for(Integer m = 0; m <= 1; m++) {
                    String time = "";
                    if (h < 10) { time = "0"; }
                     m *= 30;
                    time = time + h.toString() + ":";
                    if (m == 0) { time = time + "0"; }
                    time = time + m + ":00";
                    AzimuthElevation ae = AstroCalculation.getAzimuthElevation(date, time, this.virgo.myLatLong.latitude, this.virgo.myLatLong.longitude, Math.toRadians(s.ra), Math.toRadians(s.decl));
                    if (ae.elevation > 0) {
                        XY xy = AstroCalculation.toCartesian(ae.azimuth, ae.elevation, this.virgo.f_sd_axis / 2, this.virgo.sd_x_root, this.virgo.sd_y_root);
                        this.virgo.ticks.add(this.virgo.new sdScreenCoordinates(s.ref, -1, xy.x, xy.y));
                    }
                }
            }
        } 
    }
    
    public void mkTracks() {
        String date = DateTimeUTC.getDateUTC();
        for (Shower s: this.virgo.showers) {
            for(Integer h = 0; h <= 23; h++) {
               for(Integer m = 0; m <= 59; m++) {
                    String time = "";
                    if (h < 10) { time = "0"; }
                    time = time + h.toString() + ":";
                    if (m < 10) { time = time + "0"; }
                    time = time + m + ":00";
                    AzimuthElevation ae = AstroCalculation.getAzimuthElevation(date, time, this.virgo.myLatLong.latitude, this.virgo.myLatLong.longitude, Math.toRadians(s.ra), Math.toRadians(s.decl));
                    if (ae.elevation > 0) {
                        XY xy = AstroCalculation.toCartesian(ae.azimuth, ae.elevation, this.virgo.f_sd_axis / 2, this.virgo.sd_x_root, this.virgo.sd_y_root);
                        this.virgo.tracks.add(this.virgo.new sdScreenCoordinates(s.ref, 10, xy.x, xy.y));
                    }
                }
            }
        } 
    }

}
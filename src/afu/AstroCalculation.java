package afu;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AstroCalculation {

	/** Julian day for 2000-01-01, 12h UT (days since 01.01.4713 BC, 12 h UT) */
	private static final double JD_2000_01_01 = 2451545.0;
	/** Julian day for 0000-03-01, 00h UT (days since 01.01.4713 BC, 12 h UT) */
	private static final double JD_0000_03_01 = 1721118.5;
	/** days per century */
	private static final int d_century = 36525;
	/** hours per day */
	private static final int h_day = 24;
	/** minutes per day */
	private static final int m_day = 1440;
	/** seconds per day */
	private static final int s_day = 86400;
	/** elevation (-00° 34') for rise and set of stars [rad] */
	public static final double ele_star = Math.toRadians(-34.0 / 60.0);
	/** 2 x Pi */
	public static final double pi2 = 2.0 * Math.PI;

	public static DateTime JDtoGregorianDate(double JD) {
		// Input: Julian day [decimal days]
		// Output: Gregorian date [yyyy-mm-dd] and time [hh:mm:ss] UTC

		// Source: http://vsg.cape.com/~pbaum/date/date0.htm
		// modified to get both date and time

		double fracJD = Conversion.getFrac(JD);
		double Z = (int) (JD - JD_0000_03_01);
		double R = JD - JD_0000_03_01 - Z;
		double G = Z - 0.25;
		double A = (int) (G / 36524.25);
		double B = A - (int) (A / 4);
		int year = (int) ((B + G) / 365.25);
		double C = B + Z - (int) (365.25 * year);
		int month = (int) ((5 * C + 456) / 153);
		int day = (int) (C - (int) ((153 * month - 457) / 5) + R);
		if (month > 12) {
			year++;
			month -= 12;
		}
		String date = year + "-";
		if (month < 10) {
			date = date + "0";
		}
		date = date + month + "-";
		if (day < 10) {
			date = date + "0";
		}
		date = date + day;
		double hour = Conversion.getFracMod((fracJD + 0.5) * 24, 24);

		// -- rounding errors!
		if ((Math.abs(1 - Conversion.getFrac(hour)) < 0.0000001) || (Math.abs(Conversion.getFrac(hour) - 1) < 0.0000001)) {
			Math.round(hour);
		}
		double min = Conversion.getFrac(hour) * 60;
		if ((Math.abs(1 - Conversion.getFrac(min)) < 0.0000001) || (Math.abs(Conversion.getFrac(min) - 1) < 0.0000001)) {
			Math.round(min);
		}
		double sec = Conversion.getFrac(min) * 60;
		hour = (int) hour;
		min = (int) min;
		sec = Math.round(sec);
		String time = "";
		if (hour < 10) {
			time = "0";
		}
		time = time + (int) hour + ":";
		if (min < 10) {
			time = time + "0";
		}
		time = time + (int) min + ":";
		if (sec < 10) {
			time = time + "0";
		}
		time = time + (int) sec;

		return new DateTime(date, time);
	}

	public static double getJulianDay(String date, String time) {
		// Input: Gregorian date [yyyy-mm-dd] and time [hh:mm:ss] up from
		// 1582-10-15 (!)
		// Output: Julian day [decimal days]

		// Source: http://vsg.cape.com/~pbaum/date/date0.htm

		double JD = 0;
		int cum_days[] = { 0, 0, 0, 0, 31, 61, 92, 122, 153, 184, 214, 245, 275, 306, 337 };
		int year = 0;
		int month = 0;
		int day = 0;
		int hour = 0;
		int min = 0;
		int sec = 0;
		Pattern p;
		Matcher m;

		p = Pattern.compile("(\\d{4})-(\\d\\d)-(\\d\\d)");
		m = p.matcher(date);
		if (m.find()) {
			year = Integer.parseInt(m.group(1));
			month = Integer.parseInt(m.group(2));
			day = Integer.parseInt(m.group(3));
		}
		p = Pattern.compile("(\\d\\d):(\\d\\d):(\\d\\d)");
		m = p.matcher(time);
		if (m.find()) {
			hour = Integer.parseInt(m.group(1));
			min = Integer.parseInt(m.group(2));
			sec = Integer.parseInt(m.group(3));
		}

		// due to leap years, period starts at 01. March
		if (month < 3) {
			month += 12;
			year--;
		}
		JD = day + cum_days[month] + 365 * year + (year / 4) - (year / 100) + (year / 400) + JD_0000_03_01;
		JD += (double) hour / (double) h_day;
		JD += (double) min / (double) m_day;
		JD += (double) sec / (double) s_day;
		return JD; // [decimal days]
	}

	public static DateTime SolarLongitudetoGregorianDate(String date, double sollong) {
		// Input: Gregorian date [yyyy-mm-dd]
		// Solar longitude [rad]
		// Output: Gregorian date [yyyy-mm-dd] and time [hh:mm:ss] UTC

		// -- Source: http://wise-obs.tau.ac.il/~eran/Wise/Util/SolLon.html
		// modified for time resolution in minutes
		Pattern p = Pattern.compile("(\\d{4})-(\\d\\d)-(\\d\\d)");
		Matcher m = p.matcher(date);

		String year = "", month = "";
		if (m.find()) {
			year = m.group(1);
			month = m.group(2);
		}
		int N = Integer.valueOf(year) - 2000;
		double JDM0 = 2451182.24736 + 365.25963575 * N;

		// -- calc approximate JD
		double ApproxJD = getJulianDay(year + "-" + month + "-15", "12:00:00");
		double DiffJD = ApproxJD - JD_2000_01_01;
		double Dt = 1.94330 * Math.sin(sollong - 1.798135) + 0.01305272 * Math.sin(2.0 * sollong + 2.634232) + 78.195268
				+ 58.13165 * sollong - 0.0000089408 * DiffJD;
		if (Math.abs(ApproxJD - (JDM0 + Dt)) > 50.0) {
			Dt = Dt + 365.2596;
		}
		double JD1 = JDM0 + Dt;
		return JDtoGregorianDate(JD1);
	}

	public static double getLocalMeanSiderealTime(String date, String time,
			double longitude) {
		// Input: local observation date [yyyy-mm-dd] and time [hh:mm:ss
		// (UT)], up from 2000-01-01
		// longitude [rad], positive to the east, negative to the west
		// Output: local mean sidereal time [rad]

		// Source: Montenbruck/Pfleger, 2004, p. 40, modified (due to bugs)

		double JD = getJulianDay(date, time);
		double JD_0 = getJulianDay(date, "00:00:00"); // source: calculation of
												// JD_0, t and t_0 not
												// correct!
		double UT = (double) s_day * (JD - JD_0); // UT as fraction of a day [sec]
		double t = (JD - JD_2000_01_01) / (double) d_century; // Julian centuries
														// since 2000-01-01,
														// 12h UT
		double t_0 = (JD_0 - JD_2000_01_01) / (double) d_century;
		double gmst = 24110.54841 + 8640184.812866 * t_0 + 1.0027379093 * UT + 0.093104 - 0.0000062 * Math.pow(t, 3.0);
		return ((2.0 * Math.PI / (double) s_day) * Conversion.getFracMod(gmst, (double) s_day) + longitude); // local
																										// mean
																										// sidereal
																										// time
																										// [rad]
	}

	public static Visibility getRiseSet(String date, double latitude, double longitude, double ra, double decl) {
		// Input: Gregorian date [yyyy-mm-dd]
		// latitude, longitude, right ascension, declination [rad]
		// Output: above horizon [-], rise time, set time [rad]
		// Source: Montenbruck/Pfleger, 2004, p. 46

		double rise_time = 0;
		double set_time = 0;
		int above_horizon = 0; // rise and set over the day
		double mst_rise, mst_set;
		double lmst_00;

		double hour_angle = (Math.sin(ele_star) - (Math.sin(latitude) * Math.sin(decl)))
				/ (Math.cos(latitude) * Math.cos(decl));
		if (Math.abs(hour_angle) > 1.0) {
			above_horizon = (int) (hour_angle / Math.abs(hour_angle) * -1); // circumpolar
																			// (1)
																			// or
																			// never
																			// above
																			// the
																			// horizon
																			// for
																			// the
																			// whole
																			// day
																			// (-1)
		} else {
			hour_angle = Math.acos(hour_angle); // [rad]
			mst_rise = Conversion.getFracMod((ra - hour_angle), pi2); // [rad]
			mst_set = Conversion.getFracMod((ra + hour_angle), pi2); // [rad]
			lmst_00 = getLocalMeanSiderealTime(date, "00:00:00", longitude); // [rad]
			rise_time = Conversion.getFracMod(pi2 - (lmst_00 - mst_rise), pi2); // [rad]
			set_time = Conversion.getFracMod((mst_set - lmst_00), pi2); // [rad]
		}
		return new Visibility(above_horizon, rise_time, set_time);
	}

	public static AzimuthElevation getAzimuthElevation(String date, String time, double latitude, double longitude,
			double ra, double decl) {
		// Input: Gregorian date [yyyy-mm-dd] and time [hh:mm:ss (UT)]
		// latitude, longitude, right ascension, declination [rad]
		// Output: astronomical azimuth, elevation [rad]
		// Source: http://www.prairienet.org/cuas/faqcalc.shtml
		// Bill Owen <wmo@wansor.jpl.nasa.gov>
		double lmst = getLocalMeanSiderealTime(date, time, longitude);
		double hour_angle = lmst - ra;

		// astronomical azimuth: S 0°, W 90°, E -90°, N 180/-180°
		double azimuth = Math.atan2(Math.sin(hour_angle),
				Math.cos(hour_angle) * Math.sin(latitude) - Math.tan(decl) * Math.cos(latitude));
		double elevation = Math.asin(
				Math.sin(latitude) * Math.sin(decl) + Math.cos(latitude) * Math.cos(decl) * Math.cos(hour_angle));
		return new AzimuthElevation(azimuth, elevation);
	}

	public static XY toCartesian(double azimuth, double elevation, int r, int x_offset, int y_offset) {
		// Input: spherical data: astromical azimuth, elevation [rad]
		//		S 0°, W 90°, E -90°, N 180/-180°
		//		radius of the polar projection plane [pixel]
		// Output: xy projection data [pixel]; upper left corner is 0, 0
		//		x increases to the right, y increases downwards
		//		negative elevation of less than rise/set elevation value give negative y values
		// Source: Duffett-Smith, 1997, p. 92
		int x = (int) Math.round((r * Math.cos(elevation) * -1.0 * Math.sin(azimuth))) + r + x_offset;
		int y = (int) Math.round((r * Math.cos(elevation) * Math.cos(azimuth))) + r + y_offset;
		if (elevation < ele_star) {
			y *= -1;
		}
		return new XY(x, y);
	}
}
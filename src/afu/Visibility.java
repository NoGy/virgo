package afu;

public class Visibility {
	public int above_horizon;
	public double rise_time;
	public double set_time;

	public Visibility(int above_horizon, double rise_time, double set_time) {
		this.above_horizon = above_horizon;
		this.rise_time = rise_time;
		this.set_time = set_time;
	}
}
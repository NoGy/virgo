package afu;

public class AzimuthElevation {
    public double azimuth;
    public double elevation;
    
    public AzimuthElevation() {
    }

    public AzimuthElevation(double azimuth, double elevation) {
        this.azimuth = azimuth;
        this.elevation = elevation;
    }
}
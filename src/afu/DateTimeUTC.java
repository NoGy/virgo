package afu;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateTimeUTC {

	private DateTimeUTC() {}
	private static final SimpleDateFormat d = new SimpleDateFormat("yyyy-MM-dd");
	private static final SimpleDateFormat t = new SimpleDateFormat("HH:mm:ss");
	private static final SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final SimpleDateFormat min = new SimpleDateFormat("mm");
	static {
		TimeZone utc = TimeZone.getTimeZone("GMT-00:00");
		d.setTimeZone(utc);
		t.setTimeZone(utc);
		dt.setTimeZone(utc);
		min.setTimeZone(utc);
	}

	public static String getDateUTC() {
		return d.format(new Date());
	}

	public static String getTimeUTC() {
		return t.format(new Date());
	}

	public static String getDateTimeUTC() {
		return dt.format(new Date());
	}

	public static String getMin() {
		return min.format(new Date());
	}

	public static int getSecstofull() {
		return 60 - Calendar.getInstance().get(Calendar.SECOND);
	}
}

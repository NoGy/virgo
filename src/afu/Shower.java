package afu;

//--- Shower List ---------------------------------------------------------------------     
public class Shower {
    public Integer S_ID;
    public String name;
    public String ref;
    public String begin;
    public String end;
    public String peak_date;
    public String peak_time;
    public double ra;
    public double decl;
    public double speed;
    public String zhr;
    public int mclass;
    public String rise;
    public String set;
    public int diff_peak;

    public Shower(Integer S_ID, String name, String ref, String begin, String end, String peak_date, String peak_time, double ra, double decl, double speed, String zhr, int mclass, String rise, String set, int diff_peak) {
        this.S_ID = S_ID;
        this.name = name;
        this.ref = ref;
        this.begin = begin;
        this.end = end;
        this.peak_date = peak_date;
        this.peak_time = peak_time;
        this.ra = ra;
        this.decl = decl;
        this.speed = speed;
        this.zhr = zhr;
        this.mclass = mclass;
        this.rise = rise;
        this.set = set;
        this.diff_peak = diff_peak;
    }
 }
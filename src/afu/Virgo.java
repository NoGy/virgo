package afu;
/*
 * virgo.java
 *
 * @author sabine
 *
 * Co authored and transformed into an application by seppel
 *
 * Created on 26. November 2005, 16:35
 */

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.RenderingHints;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;

public class Virgo extends JPanel implements ActionListener {
	private Font logofont;
	private Font descfont;
	private Font datefont;
	private Font timefont;
	private Font headerfont;
	private Font textfont;
	private Font btextfont;
	private Font sd_dirfont;
	private Font sd_degfont;
	private Font sd_reffont;
	private Font ai_dirfont;
	private Font ai_degfont;
	private Font tablefont;
	private Font btablefont;
	final static BasicStroke stroke = new BasicStroke(1.0f);
	final static BasicStroke wideStroke = new BasicStroke(8.0f);
	final static float dotted1[] = { 2.0f };
	final static BasicStroke dotted = new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f,
			dotted1, 0.0f);
	final static float dashed1[] = { 10.0f };
	final static BasicStroke dashed = new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f,
			dashed1, 0.0f);
	final static BasicStroke aineedle = new BasicStroke(5.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
	final static BasicStroke p2 = new BasicStroke(2.0f);
	final static BasicStroke p3 = new BasicStroke(3.0f);
	final static BasicStroke p5 = new BasicStroke(5.0f);
	final static BasicStroke p8 = new BasicStroke(8.0f);
	final static BasicStroke p10 = new BasicStroke(10.0f);
	final static BasicStroke p12 = new BasicStroke(12.0f);
	final static Color bgreen = new Color(3, 252, 165);
	final static Color wred = new Color(228, 28, 48);
	private String dateonstart;
	private String osname;
	private String myID;
	private String myCall;
	private String myLocator;
	private boolean cookie;
	private FontMetrics fontmetrics;
	private int strwidth;
	private int namewidth;
	private Graphics2D offscreenGraphics;
	private Image offscreenImage;
	private Dimension dimension;
	private ArrayList<String> dbRetrieval;
	private String[] d;
	private Image oswin_monitor;
	LatLong oswinLatLong;
	private Integer oswin_events;
	private Boolean oswin_online;
	private String oswin_msg;
	private int oswin_update = 10;

	private DBclient dbClient;

	LatLong myLatLong;

	// public class Ovals {
	// public ArrayList<XY> drawCircle(int numPoints, int width, int height) {
	// double radians = 0.0;
	// ArrayList<XY> xy = new ArrayList<XY>();
	//
	// for (int i=0; i<numPoints; i++) {
	// xy = add(new XY(width, height));
	// xPoints[i] = width * Math.cos(radians);
	// yPoints[i] = height * Math.sin(radians);
	// radians += (2.0 * Math.PI) / numPoints;
	// }
	// }
	//
	// }

	ArrayList<Shower> showers;
	// --- Shower List
	// ---------------------------------------------------------------------

	// --- Control panel
	private int cp_x_root = 0;
	private int cp_y_root = 0;
	private int cp_width = 140;
	private int cp_height;
	// --- Control panel

	// --- Sky Display
	// ---------------------------------------------------------------------
	int sd_x_root = cp_x_root + cp_width + 35; // minimum is 21 (lettering)
	int sd_y_root = 38; // minimum is 8 (lettering)
	private int sd_axis = 180; // axis is two times 90° long (180 pixel)
	private int sd_f = 2; // projection factor is 2
	int f_sd_axis = sd_f * sd_axis; // 360 pixel

	public class sdScreenCoordinates {
		private Graphics g;
		public String ref;
		public int x;
		public int y;
		public int dia_rad;
		public Color col_rad;
		private int x_ref;
		private int y_ref;
		public Rectangle2D shape_ref;

		public sdScreenCoordinates(String _ref, int _mclass, int _x, int _y) {
			this.ref = _ref;
			this.x = _x;
			this.y = _y;

			dia_rad = 10;
			switch (_mclass) {
			case 0:
				col_rad = Color.magenta;
				break;
			case 1:
				col_rad = wred;
				break;
			case 2:
				col_rad = Color.orange;
				break;
			case 3:
				col_rad = Color.yellow;
				break;
			case 4:
				col_rad = bgreen;
				break;

			// -- maxima
			case 1000:
				dia_rad = 14;
				col_rad = Color.orange;
				break;

			// -- tracks
			case 10:
				dia_rad = 0;
				col_rad = Color.cyan;
				break;

			// -- ticks
			default:
				dia_rad = 4;
				col_rad = Color.cyan;
				break;
			}
			if (dia_rad == 0) {
				x -= 1;
				y -= 1;
			} else {
				x -= dia_rad / 2;
				y -= dia_rad / 2;
				shape_ref = getFontMetrics(getFont()).getStringBounds(ref, g);
				if ((x + dia_rad / 2) < (f_sd_axis / 2 + sd_x_root)) {
					x_ref = x + dia_rad + 5;
				} else {
					x_ref = x - (int) shape_ref.getWidth() - 7;
				}
				if ((y + dia_rad / 2) < (f_sd_axis / 2 + sd_y_root)) {
					y_ref = y + 5 + (int) shape_ref.getHeight();
				} else {
					y_ref = y + 5;
				}
				shape_ref.setFrame(x_ref - 3, y_ref - shape_ref.getHeight(), shape_ref.getWidth() + 8,
						shape_ref.getHeight() + 5);
			}
		}
	}

	ArrayList<sdScreenCoordinates> radiants;
	ArrayList<sdScreenCoordinates> oswin_radiants;
	ArrayList<sdScreenCoordinates> ticks;
	ArrayList<sdScreenCoordinates> tracks;
	ArrayList<sdScreenCoordinates> maxima;

	// --- Azimuth indicator
	public class AzimuthIndicator {
		private final Virgo virgo;

		public class AiScreenCoordinates {
			private Color col_rad;
			private int x1, y1, x2, y2;
		
			public AiScreenCoordinates(Color col_rad, int x1, int y1, int x2, int y2) {
				this.col_rad = col_rad;
				this.x1 = x1;
				this.y1 = y1;
				this.x2 = x2;
				this.y2 = y2;
			}
		}

		private AzimuthElevation ae = new AzimuthElevation();
		private XY xy = new XY();
		private double azimuth;
		private Color col_rad;
		private int x1, y1, x2, y2;

		public AzimuthIndicator(Virgo virgo) {
			this.virgo = virgo;
		}

		public void mkQTF() {
			for (Shower s : this.virgo.showers) {

				// Only major showers of class 1 and 0 visible on azimuth
				// indicator
				if (!this.virgo.onlymajor || (this.virgo.onlymajor && s.mclass < 2)) {
					ae = AstroCalculation.getAzimuthElevation(DateTimeUTC.getDateUTC(), DateTimeUTC.getTimeUTC(),
							this.virgo.myLatLong.latitude, this.virgo.myLatLong.longitude, Math.toRadians(s.ra), Math.toRadians(s.decl));

					azimuth = ae.azimuth;
					// add 90° for antenna azimuth
					if (this.virgo.antenna) {
						azimuth = Conversion.getFracMod((azimuth + (Math.PI / 2)), Math.PI * 2);
					}
					for (sdScreenCoordinates sc : this.virgo.radiants) {
						if (sc.ref.equals(s.ref)) {
							col_rad = sc.col_rad;
						}
					}
					xy = AstroCalculation.toCartesian(azimuth, Math.toRadians(45) * (ae.elevation / Math.abs(ae.elevation)),
							this.virgo.f_ai_axis / 2, this.virgo.ai_x_root, this.virgo.ai_y_root);
					x1 = xy.x;
					y1 = xy.y;

					// shift 180°
					azimuth = azimuth - (Math.PI * azimuth / Math.abs(azimuth));

					xy = AstroCalculation.toCartesian(azimuth, Math.toRadians(45) * (ae.elevation / Math.abs(ae.elevation)),
							this.virgo.f_ai_axis / 2, this.virgo.ai_x_root, this.virgo.ai_y_root);
					x2 = xy.x;
					y2 = xy.y;
					qtf.add(new AiScreenCoordinates(col_rad, x1, y1, x2, y2));
				}
			}
		}

		public void mkTicks() {

			for (double i = 0.0; i <= 350.0; i = i + 30.0) {
				int x = (int) (-(this.virgo.f_ai_axis / 3 + 10) * Math.sin(Math.toRadians(i))) + this.virgo.f_ai_axis / 3 + 10;
				int y = (int) ((this.virgo.f_ai_axis / 3 + 10) * Math.cos(Math.toRadians(i))) + this.virgo.f_ai_axis / 3 + 10;
				this.virgo.compasscard_30.add(new XY(x, y));
			}
			for (double i = 0.0; i <= 350.0; i = i + 10.0) {
				int x = (int) (-(this.virgo.f_ai_axis / 3 + 10) * Math.sin(Math.toRadians(i))) + this.virgo.f_ai_axis / 3 + 10;
				int y = (int) ((this.virgo.f_ai_axis / 3 + 10) * Math.cos(Math.toRadians(i))) + this.virgo.f_ai_axis / 3 + 10;
				if (Conversion.getFrac(i / 30.0) != 0) {
					this.virgo.compasscard_10.add(new XY(x, y));
				}
			}
			for (double i = 5.0; i <= 355.0; i = i + 10.0) {
				int x = (int) (-(this.virgo.f_ai_axis / 3 + 5) * Math.sin(Math.toRadians(i))) + this.virgo.f_ai_axis / 3 + 5;
				int y = (int) ((this.virgo.f_ai_axis / 3 + 5) * Math.cos(Math.toRadians(i))) + this.virgo.f_ai_axis / 3 + 5;
				this.virgo.compasscard_5.add(new XY(x, y));
			}
		}
	}
	// ---------------------------------------------------------------
	private int ai_axis = 180; // axis is two times 90° long (180 pixel)
	private double ai_f = 1.5; // projection factor is 1.5
	int f_ai_axis = (int) (ai_f * ai_axis); // 270 pixel
	int ai_x_root = f_sd_axis + sd_x_root + 60;
	int ai_y_root = f_sd_axis - f_ai_axis + sd_y_root;
	List<XY> compasscard_30;
	List<XY> compasscard_10;
	List<XY> compasscard_5;

	ArrayList<AzimuthIndicator.AiScreenCoordinates> qtf;
	// --- Azimuth indicator
	// ---------------------------------------------------------------

	// -- OSWIN monitor
	int os_x_root = cp_x_root + cp_width + 20;
	int os_y_root = sd_y_root + f_sd_axis + 35;
	// -- OSWIN monitor

	// --- Table
	// ---------------------------------------------------------------------------
	private int t_x_root = cp_x_root + cp_width + 20;
	// private int t_y_root = sd_y_root + f_sd_axis + 175;
	private int t_y_root = sd_y_root + f_sd_axis + 70;

	// --- Table
	// ---------------------------------------------------------------------------

	private SkyDisplay skydisplay;

	private AzimuthIndicator azimuthindicator;

	public class RecentShowers {

		public void getShowers() {
			// -- 1 2
			// -- o--- +d1 --->|-------------------------->|
			// -- o--- +d2 ------------------------------->|
			// --
			// -- |<-- -d1 ---o--- +d2 ------>|
			// --
			// -- | |<---- -d2 ---o
			// -- |<-------------------------------- -d1 ---o
			// -- a1, d1 a2, d2
			// --
			// -- alpha = a1 + ((a2 - a1) / (d2 - d1) * d1 * -1)

			String[] d;
			String[] first = {};
			String[] second = {};
			DateTime peak;
			int diff_begin;
			int diff_end;
			int diff_peak;

			dbRetrieval.clear();
			dbRetrieval = dbClient.retrieveData(
					"select shower.S_ID, name, ref, begin, end, max, sollong, v, zhr, class, datediff(max, begin), datediff(end, max), datediff('"
							+ dateonstart
							+ "', max) as diff_peak from activity left join shower using (S_ID) where date('"
							+ dateonstart + "') between begin and end order by class, abs(datediff(max, '" + dateonstart
							+ "'))");
			System.out.println(dbRetrieval);
			// Virgo version 2.21 2014-02-15
			// [78;Lyrids;LYR;2016-04-16;2016-04-25;2016-04-22;32.32;49;18;1;6;3;0,
			// 52;Eta-Aquarids;ETA;2016-04-19;2016-05-28;2016-05-06;45.5;66;40,
			// ;1;17;22;-14, 136;Antihelion Source;ANT;2016-01-01;2016-12-31;
			// ;0;9999;4;2;;; ,
			// 105;Pi-Puppids;PPU;2016-04-15;2016-04-28;2016-04-23;33.5;18;var;3;8;5;-1,
			// 139;April Piscids
			// Daytime;DAP;2016-04-08;2016-04-29;2016-04-20;32.5;9999;--;4;12;9;2]
			// s:
			// 78;Lyrids;LYR;2016-04-16;2016-04-25;2016-04-22;32.32;49;18;1;6;3;0
			// s:
			// 52;Eta-Aquarids;ETA;2016-04-19;2016-05-28;2016-05-06;45.5;66;40

			for (String s : dbRetrieval) {
				// TEST
				System.out.println("s: " + s);
				d = s.split(";");

				// -- Antihelion Source
				if (Integer.valueOf(d[0]) == 136) {
					showers.add(new Shower(Integer.valueOf(d[0]), d[1], d[2], d[3], d[4], "1800-01-01", "00:00", 0.0,
							0.0, 0.0, d[8], Integer.valueOf(d[9]), "", "", 0));
				} else {
					peak = AstroCalculation.SolarLongitudetoGregorianDate(d[5], Math.toRadians(Double.valueOf(d[6])));

					// -- distance in percent from now to peak activity
					if (Integer.valueOf(d[12]) < 0) {
						diff_peak = (int) Math.round(100.0 / Integer.valueOf(d[10]) * Integer.valueOf(d[12]));
					} else {
						diff_peak = (int) Math.round(100.0 / Integer.valueOf(d[11]) * Integer.valueOf(d[12]));
					}

					// forcing correct display
					if (Integer.valueOf(d[12]) == -1) {
						diff_peak = -5;
					}
					if (Integer.valueOf(d[12]) == 1) {
						diff_peak = 5;
					}
					// showers.add(new Shower(Integer.valueOf(d[0]), d[1], d[2],
					// d[3], d[4], d[5], conv.toShortTime(peak.time), 0.0, 0.0,
					// Double.valueOf(d[7]), d[8], Integer.valueOf(d[9]), "",
					// "", diff_peak));
					showers.add(new Shower(Integer.valueOf(d[0]), d[1], d[2], d[3], d[4], peak.date,
							Conversion.toShortTime(peak.time), 0.0, 0.0, Double.valueOf(d[7]), d[8], Integer.valueOf(d[9]),
							"", "", diff_peak));
				}
			}

			for (Shower s : showers) {
				// -- look for celestial data before today
				dbRetrieval.clear();
				dbRetrieval = dbClient.retrieveData(
						"select back, alpha, delta, date from (select distinct datediff(date, '" + dateonstart
								+ "') as back, alpha, delta, date from celestial left join activity using (S_ID) where celestial.s_id="
								+ s.S_ID + " && date >= begin && date < date('" + dateonstart
								+ "') order by date desc limit 1) as b where back >= -50");

				// -- found at least one dataset in the nearest past
				if (!dbRetrieval.isEmpty()) {
					first = dbRetrieval.get(0).split(";");
					dbRetrieval.clear();
					// TEST
					// System.out.println(s.ref + ": Found one dataset
					// backwards");
					// look for a dataset in the future
					dbRetrieval = dbClient.retrieveData("select distinct datediff(date, '" + dateonstart
							+ "') as forward, alpha, delta, date from celestial left join activity using (S_ID) where celestial.s_id="
							+ s.S_ID + " && date <= end && date >= date('" + dateonstart + "') order by date limit 1");
					if (!dbRetrieval.isEmpty()) {
						second = dbRetrieval.get(0).split(";");
						// TEST
						// System.out.println("Found second dataset ahead
						// (interpolating)");
						// if we don't have one, we take two in the right order
						// from the past
					} else {
						// TEST
						// System.out.println(s.ref + ": Got two datasets
						// backwards (extrapolating forward)");
						dbRetrieval.clear();
						dbRetrieval = dbClient.retrieveData(
								"select back, alpha, delta, date from (select distinct datediff(date, '" + dateonstart
										+ "') as back, alpha, delta, date from celestial left join activity using (S_ID) where celestial.s_id="
										+ s.S_ID + " && date >= begin && date <= date('" + dateonstart
										+ "') order by date desc limit 2) as b where back >= -50");
						first = dbRetrieval.get(1).split(";");
						second = dbRetrieval.get(0).split(";");
					}
				} else {
					// TEST
					// System.out.println(s.ref + ": Got two datasets ahead
					// (extrapolating backwards)");
					// -- if there is no element before today, we need the
					// nearest datasets of the future
					dbRetrieval.clear();
					dbRetrieval = dbClient.retrieveData("select distinct datediff(date, '" + dateonstart
							+ "') as forward, alpha, delta, date from celestial left join activity using (S_ID) where celestial.s_id="
							+ s.S_ID + " && date <= end && date >= date('" + dateonstart + "') order by date limit 2");
					if (!dbRetrieval.isEmpty() && dbRetrieval.size()>1) {
						first = dbRetrieval.get(0).split(";");
						second = dbRetrieval.get(1).split(";");
					}
				}
				// ra = alpha_1 + ((alpha_2 - alpha_1) / (datediff_2 -
				// datediff_1)) * date_diff_1 * -1
				s.ra = Math.round(Double.valueOf(first[1]) + ((Double.valueOf(second[1]) - Double.valueOf(first[1]))
						/ (Double.valueOf(second[0]) - Double.valueOf(first[0])) * Double.valueOf(first[0]) * -1));
				s.decl = Math.round(Double.valueOf(first[2]) + ((Double.valueOf(second[2]) - Double.valueOf(first[2]))
						/ (Double.valueOf(second[0]) - Double.valueOf(first[0])) * Double.valueOf(first[0]) * -1));
				// TEST
				// System.out.println(s.ref + " " + first[3] + " (1) " +
				// second[3] + " (2), " + " RA: " + first[1] + " " + second[1] +
				// " " + s.ra + ", Decl: " + first[2] + " " + second[2] + " " +
				// s.decl);
			}
		}

		public void calcRiseSet() {
			for (Shower s : showers) {
				Visibility visibility = AstroCalculation.getRiseSet(DateTimeUTC.getDateUTC(), myLatLong.latitude, myLatLong.longitude,
						Math.toRadians(s.ra), Math.toRadians(s.decl));
				if (visibility.above_horizon == 0) {
					s.rise = Conversion.toShortSolarTime(visibility.rise_time);
					s.set = Conversion.toShortSolarTime(visibility.set_time);
				} else {
					if (visibility.above_horizon == 1) {
						s.rise = "circumpolar";
					} else {
						s.rise = "not visible";
					}
				}
			}
		}
	}

	private RecentShowers recentshowers;

	// ---TIMER----------------------------------------------------------------------

	class Task implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String date;
			int min;

			date = DateTimeUTC.getDateUTC();
			if (!date.equals(dateonstart)) {
				dateonstart = date;
				showers.clear();
				recentshowers.getShowers();
				if (!showers.isEmpty()) {
					recentshowers.calcRiseSet();
					ticks.clear();
					skydisplay.mkTicks();
					maxima.clear();
					skydisplay.mkMaxima();
					namewidth = fontmetrics.stringWidth("Meteor Shower") + 15;
					for (Shower s : showers) {
						strwidth = fontmetrics.stringWidth(s.name);
						if (strwidth > namewidth) {
							namewidth = strwidth;
						}
					}
					namewidth += 15;
				}
			}
			radiants.clear();
			skydisplay.mkRadiants();
			// oswin_radiants.clear();
			// skydisplay.mkoswinRadiants();
			qtf.clear();
			azimuthindicator.mkQTF();
			/*
			 * if (oswin_update != 0) { min =
			 * Integer.parseInt(datetimeUTC.getMin()); if ((oswin_update == 60
			 * && min == 8) || (oswin_update == 10 && (min == 8 || min == 18 ||
			 * min == 28 || min == 38 || min == 48 || min == 58))) {
			 * oswin_monitor.flush(); //oswin_monitor =
			 * getImage(getCodeBase(),"test.png"); oswin_monitor =
			 * getImage(getCodeBase(),"OSWIN/oswin_monitor.png");
			 * 
			 * //-- without mediatracker there is a timing problem on timer
			 * update! mt.addImage(oswin_monitor,0); try { mt.waitForAll(); }
			 * catch (InterruptedException e) { } dbRetrieval.clear();
			 * dbRetrieval = dbClient.
			 * retrieveData("select oswin_1h from meteorradar where date_utc-utc_timestamp>-15000 && oswin_1h<777777"
			 * ); if (dbRetrieval.isEmpty()) { oswin_online = false; oswin_msg =
			 * "OSWIN radar is down due to maintenance works"; } else {
			 * dbRetrieval.clear(); dbRetrieval = dbClient.
			 * retrieveData("select oswin_1h from meteorradar order by date_utc desc limit 2"
			 * ); oswin_events = Integer.parseInt(dbRetrieval.get(0)); if
			 * (Integer.parseInt(dbRetrieval.get(0)) >= 777777) { oswin_events =
			 * Integer.parseInt(dbRetrieval.get(1)); } if (oswin_events <
			 * 777777) { oswin_online = true; oswin_msg = " "; } else {
			 * oswin_online = false; oswin_msg =
			 * "Data stream temporarily interrupted"; } }
			 * 
			 * } }
			 */
			offpaint();
			repaint();
		}
	}
	// ---TIMER----------------------------------------------------------------------

	// ---Eventhandler
	// call/locator/TMSP--------------------------------------------------
	public void actionPerformed(ActionEvent evt) {
		Object source = evt.getSource();
		if (source == btnLocator || source == tfLocator || source == tfCall) {
			myLatLong = Maidenhead.toRadians(tfLocator.getText());
			// myCall = tfCall.getText();
			myLocator = tfLocator.getText();
			/*
			 * if (cbSave.getState() == true) { if (cookie &&
			 * !myCall.equals("DL1DBC")) { dbRetrieval =
			 * dbClient.retrieveData("update user set call='" + myCall +
			 * "', locator='" + myLocator + "' where id='" + myID + "'"); } }
			 */
			if (!showers.isEmpty()) {
				recentshowers.calcRiseSet();
				ticks.clear();
				skydisplay.mkTicks();
				radiants.clear();
				skydisplay.mkRadiants();
				maxima.clear();
				skydisplay.mkMaxima();
				qtf.clear();
				azimuthindicator.mkQTF();
			}
			offpaint();
			repaint();
			// -- TMSP
		} else if (source == btnTMSP) {
			// TODO: make clickable
			JOptionPane.showMessageDialog(this, "http://dl1dbc.net/Meteorscatter/TMSP/tmsp.exe", "Info", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	// ---Eventhandler
	// call/locator/TMSP--------------------------------------------------

	// ---Eventhandler sky display/azimuth controller/OSWIN
	// monitor--------------------------------
	boolean we;
	private boolean tracking;
	private boolean labels;
	boolean antenna;
	boolean onlymajor;

	ItemListener cbListener = new ItemListener() {
		public void itemStateChanged(ItemEvent evt) {
			Object source = evt.getSource();
			if (source == cbWE) {
				we = false;
				if (cbWE.isSelected())
					we = true;
				if (!showers.isEmpty()) {
					// recentshowers.calcRiseSet();
					// ticks.clear();
					// skydisplay.mkTicks();
					radiants.clear();
					skydisplay.mkRadiants();
					// oswin_radiants.clear();
					// skydisplay.mkoswinRadiants();
					maxima.clear();
					skydisplay.mkMaxima();
				}
			}
			if (source == cbTicks) {
				tracking = false;
				if (cbTicks.isSelected())
					tracking = true;
			} else if (source == cbLabels) {
				labels = false;
				if (cbLabels.isSelected())
					labels = true;
			} else if (source == cbAntenna) {
				antenna = true;
				qtf.clear();
				azimuthindicator.mkQTF();
			} else if (source == cbTrail) {
				antenna = false;
				qtf.clear();
				azimuthindicator.mkQTF();
			} else if (source == cbMajor) {
				onlymajor = false;
				if (cbMajor.isSelected())
					onlymajor = true;
				qtf.clear();
				azimuthindicator.mkQTF();

				/*
				 * } else if (source == cb10min) { oswin_update = 10; } else if
				 * (source == cb1hour) { oswin_update = 60; } else if (source ==
				 * cbNone) { oswin_update = 0;
				 */
			}

			offpaint();
			repaint();
		}
	};
	// ---Eventhandler sky display/azimuth controller/OSWIN
	// monitor--------------------------------

	private TextField tfCall;
	private TextField tfLocator;
	private JButton btnLocator;
	private JButton btnTMSP;
	private JCheckBox cbWE;
	private JCheckBox cbTicks;
	private JCheckBox cbLabels;
	private JCheckBox cbSave;
	private ButtonGroup cbg;
	private JRadioButton cbAntenna;
	private JRadioButton cbTrail;
	private JCheckBox cbMajor;
	private ButtonGroup cbo;
	private JCheckBox cb10min;
	private JCheckBox cb1hour;
	private JCheckBox cbNone;
	private MediaTracker mt;

	void onResize() {
		Dimension newSize = getSize();
		if (newSize.height <= 0 || newSize.width <= 0)
			return;
		if (!newSize.equals(dimension)) {
			System.out.println("New Size: " + newSize);
			dimension = newSize;
			cp_height = dimension.height;
				offscreenImage = createImage(dimension.width, dimension.height);
			if (offscreenImage != null) {
				offscreenGraphics = (Graphics2D) offscreenImage.getGraphics();
				RenderingHints rh = new RenderingHints(
						RenderingHints.KEY_ANTIALIASING,
						RenderingHints.VALUE_ANTIALIAS_ON);
				offscreenGraphics.setRenderingHints(rh);
				offpaint();
				repaint();
			}
		}
	}

	public static void main(String[] args) {
		// make it look better
		try {
	        UIManager.setLookAndFeel(
	                UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			// ignore
		}

		SwingUtilities.invokeLater(() -> {
			JFrame frame = new JFrame();
			frame.setTitle("Virgo version 2.23 2024-06-25");
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setSize(900, 636);
			frame.setResizable(false);

			Virgo virgo = new Virgo();
			frame.add(virgo);
			virgo.init();

			frame.setVisible(true);
		});
	}

	public void init() {
		oswin_msg = " ";
		mt = new MediaTracker(this);
		// oswin_monitor = getImage(getCodeBase(),"test.png");
		// oswin_monitor = getImage(getCodeBase(),"OSWIN/oswin_monitor.png");
		// mt.addImage(oswin_monitor,0);
		try {
			mt.waitForAll();
		} catch (InterruptedException e) {
		}
		osname = System.getProperty("os.name");
		if (osname.startsWith("Windows")) {
			logofont = new Font("verdana", Font.BOLD, 20);
			descfont = new Font("times new roman", Font.ITALIC, 15);
			datefont = new Font("verdana", Font.PLAIN, 14);
			timefont = new Font("verdana", Font.PLAIN, 14);
			headerfont = new Font("verdana", Font.PLAIN, 12);
			textfont = new Font("verdana", Font.PLAIN, 11);
			btextfont = new Font("verdana", Font.BOLD, 11);
			sd_reffont = new Font("verdana", Font.BOLD, 12);
			sd_dirfont = new Font("verdana", Font.BOLD, 18);
			sd_degfont = new Font("verdana", Font.PLAIN, 12);
			ai_dirfont = new Font("verdana", Font.BOLD, 12);
			ai_degfont = new Font("verdana", Font.BOLD, 12);
			tablefont = new Font("verdana", Font.PLAIN, 13);
			btablefont = new Font("verdana", Font.BOLD, 13);
		} else {
			logofont = new Font("sansserif", Font.BOLD, 20);
			descfont = new Font("serif", Font.ITALIC, 13);
			datefont = new Font("sansserif", Font.PLAIN, 14);
			timefont = new Font("sansserif", Font.PLAIN, 14);
			headerfont = new Font("sansserif", Font.PLAIN, 12);
			textfont = new Font("sansserif", Font.PLAIN, 11);
			btextfont = new Font("sansserif", Font.BOLD, 11);
			sd_reffont = new Font("sansserif", Font.BOLD, 12);
			sd_dirfont = new Font("sansserif", Font.BOLD, 18);
			sd_degfont = new Font("sansserif", Font.PLAIN, 12);
			ai_dirfont = new Font("sansserif", Font.BOLD, 12);
			ai_degfont = new Font("sansserif", Font.BOLD, 12);
			tablefont = new Font("sanserif", Font.PLAIN, 13);
			btablefont = new Font("sanserif", Font.BOLD, 13);
		}
		dbClient = new DBclient();
		myCall = "DL1DBC";
		myLocator = "JO31oj";
		cookie = false;
		myID = "x";

		// -- try to get the ID as parameter
//		if (getParameter("ID") != null)
//			myID = getParameter("ID");
		dbRetrieval = new ArrayList<String>();

		// -- second chance to get it via dbServer.pl (works nice with IE)
		if (myID.equals("x")) {
			dbRetrieval = dbClient.retrieveData("get ID");
			if (!dbRetrieval.isEmpty())
				myID = dbRetrieval.get(0);
			// -- System.out.println("ID: " + myID);
		}
		/*
		 * if (!myID.equals("x")) {
		 * dbClient.retrieveData("update user set lastvisit='" +
		 * datetimeUTC.getDateTimeUTC() + "' where id='" + myID + "'");
		 * dbRetrieval =
		 * dbClient.retrieveData("select call, locator from user where id='" +
		 * myID + "'"); if (!dbRetrieval.isEmpty()) { d =
		 * dbRetrieval.get(0).split(";"); cookie = true; if (d[0].length() > 2)
		 * myCall = d[0]; if (d[1].length() > 2) myLocator = d[1]; } }
		 */
		myLatLong = Maidenhead.toRadians(myLocator);
		// oswinLatLong = new LatLong(Math.toRadians(54.1),
		// Math.toRadians(11.8));

		fontmetrics = getFontMetrics(tablefont);
		dateonstart = DateTimeUTC.getDateUTC();
		showers = new ArrayList<Shower>();
		radiants = new ArrayList<sdScreenCoordinates>();
		// oswin_radiants = new ArrayList<sdScreenCoordinates>();
		ticks = new ArrayList<sdScreenCoordinates>();
		maxima = new ArrayList<sdScreenCoordinates>();
		qtf = new ArrayList<AzimuthIndicator.AiScreenCoordinates>();
		compasscard_30 = new ArrayList<>();
		compasscard_10 = new ArrayList<>();
		compasscard_5 = new ArrayList<>();

		recentshowers = new RecentShowers();
		skydisplay = new SkyDisplay(this);
		azimuthindicator = new AzimuthIndicator(this);
		azimuthindicator.mkTicks();

		setLayout(null);

		/*
		 * tfCall = new TextField(myCall);
		 * tfCall.setBackground(Color.lightGray); add(tfCall);
		 * tfCall.addActionListener(this);
		 */
		tfLocator = new TextField(myLocator);
		add(tfLocator);
		tfLocator.setBackground(Color.lightGray);
		tfLocator.addActionListener(this);

		btnLocator = new JButton("GO");
//		btnLocator.setBackground(Color.blue);
//		btnLocator.setForeground(Color.white);
		add(btnLocator);
		btnLocator.addActionListener(this);

		// -- TMSP
		/*
		 * btnTMSP = new Button("Download"); btnTMSP.setBackground(Color.blue);
		 * btnTMSP.setForeground(Color.white); add(btnTMSP);
		 * btnTMSP.addActionListener(this);
		 */

		we = false;
		tracking = true;
		labels = true;
		antenna = true;
		onlymajor = false;
		cbWE = new JCheckBox("", false);
		cbTicks = new JCheckBox("", true);
		cbLabels = new JCheckBox("", true);
		// cbSave = new Checkbox("", false);
		cbWE.setBackground(Color.lightGray);
		cbTicks.setBackground(Color.lightGray);
		cbLabels.setBackground(Color.lightGray);
		// cbSave.setBackground(Color.lightGray);
		cbWE.setFocusable(false);
		cbTicks.setFocusable(false);
		cbLabels.setFocusable(false);
		// cbSave.setFocusable(false);
		add(cbWE);
		add(cbTicks);
		add(cbLabels);
		// add(cbSave);
		cbWE.addItemListener(cbListener);
		cbTicks.addItemListener(cbListener);
		cbLabels.addItemListener(cbListener);
		// cbSave.addItemListener(cbListener);
		// -- azimuth indicator
		cbg = new ButtonGroup();
		cbAntenna = new JRadioButton("Antenna", true);
		cbTrail = new JRadioButton("Meteor trails", false);
		cbg.add(cbAntenna);
		cbg.add(cbTrail);

		cbMajor = new JCheckBox("", false);
//		cbAntenna.setBackground(Color.lightGray);
//		cbTrail.setBackground(Color.lightGray);
//		cbMajor.setBackground(Color.lightGray);
//		cbAntenna.setForeground(Color.black);
//		cbAntenna.setFocusable(false);
//		cbTrail.setFocusable(false);
//		cbMajor.setFocusable(false);
		add(cbAntenna);
		add(cbTrail);
		add(cbMajor);
		cbAntenna.addItemListener(cbListener);
		cbTrail.addItemListener(cbListener);
		cbMajor.addItemListener(cbListener);
		// -- OSWIN monitor
		/*
		 * cbo = new CheckboxGroup(); cb10min = new Checkbox("", cbo, true);
		 * cb1hour = new Checkbox("", cbo, false); cbNone = new Checkbox("",
		 * cbo, false); cb10min.setBackground(Color.lightGray);
		 * cb1hour.setBackground(Color.lightGray);
		 * cbNone.setBackground(Color.lightGray); cb10min.setFocusable(false);
		 * cb1hour.setFocusable(false); cbNone.setFocusable(false);
		 * cb10min.addItemListener(cbListener);
		 * cb1hour.addItemListener(cbListener);
		 * cbNone.addItemListener(cbListener); add(cb10min); add(cb1hour);
		 * add(cbNone);
		 */
		Timer swingtimer = new Timer(1000, new Task()); // update GUI every second
		swingtimer.start();
		setBackground(Color.black);

		// -- position of controls
		// tfCall.setBounds(cp_x_root + 15, sd_y_root + f_sd_axis / 2 + 102, 95,
		// 20);
		// tfLocator.setBounds(cp_x_root + 15, sd_y_root + f_sd_axis / 2 + 151,
		// 65, 20);
		tfLocator.setBounds(cp_x_root + 15, sd_y_root + f_sd_axis / 2 + 8, 65, 20);
		// btnLocator.setBounds(cp_x_root + 82, sd_y_root + f_sd_axis / 2 + 151,
		// 38, 20);
		btnLocator.setBounds(cp_x_root + 82, sd_y_root + f_sd_axis / 2 + 8, 38, 20);
		// -- TMSP
		// btnTMSP.setBounds(os_x_root, os_y_root + 292, 70, 20);
		// cbSave.setBounds(cp_x_root + 15, sd_y_root + f_sd_axis / 2 + 178, 15,
		// 14);
		cbWE.setBounds(cp_x_root + 15, os_y_root - 110, 15, 14);
		cbTicks.setBounds(cp_x_root + 15, os_y_root - 90, 15, 14);
		cbLabels.setBounds(cp_x_root + 15, os_y_root - 70, 15, 14);
		cbAntenna.setBounds(cp_x_root + 15, os_y_root - 10, 15, 14);
		cbTrail.setBounds(cp_x_root + 15, os_y_root + 10, 15, 14);
		cbMajor.setBounds(cp_x_root + 15, os_y_root + 30, 15, 14);
		/*
		 * cb10min.setBounds(cp_x_root + 15, os_y_root + 240, 15, 14);
		 * cb1hour.setBounds(cp_x_root + 15, os_y_root + 260, 15, 14);
		 * cbNone.setBounds(cp_x_root + 15, os_y_root + 280, 15, 14);
		 */
		recentshowers.getShowers();
		if (!showers.isEmpty()) {
			recentshowers.calcRiseSet();
			skydisplay.mkRadiants();
			// skydisplay.mkoswinRadiants();
			skydisplay.mkMaxima();
			skydisplay.mkTicks();
			azimuthindicator.mkQTF();
			namewidth = fontmetrics.stringWidth("Meteor Shower") + 15;
			for (Shower s : showers) {
				strwidth = fontmetrics.stringWidth(s.name);
				if (strwidth > namewidth) {
					namewidth = strwidth;
				}
			}
			namewidth += 15;
		}
		/*
		 * dbRetrieval.clear(); oswin_online = false; dbRetrieval = dbClient.
		 * retrieveData("select oswin_1h from meteorradar order by date_utc desc limit 2"
		 * ); oswin_events = Integer.parseInt(dbRetrieval.get(0)); if
		 * (Integer.parseInt(dbRetrieval.get(0)) >= 777777) { oswin_events =
		 * Integer.parseInt(dbRetrieval.get(1)); } if (oswin_events < 777777) {
		 * oswin_online = true; }
		 */
		// -- while waiting for timer to start:
		this.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				onResize();
			}
		});
		onResize();
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		if (offscreenImage != null)
			g2.drawImage(offscreenImage, 0, 0, this);
	}

	public void offpaint() {
		Graphics2D g2 = offscreenGraphics;
		int x_str = (sd_x_root + f_sd_axis / 2) - 7;
		int x_rect = (sd_x_root + f_sd_axis / 2) - 9;
		int rect_width = 20;
		int strwidth;
		int vertspace = 19;
		int ev_offset = 0;
		int i;

		if (g2 == null) {
			onResize();
			offpaint();
			return;
		}
		if (dimension == null)
			dimension = getSize();
		g2.clearRect(0, 0, dimension.width, dimension.height);
		g2.setColor(Color.black);
		g2.fillRect(0, 0, dimension.width, dimension.height);

		// --------------Control
		// panel-------------------------------------------

		g2.setColor(Color.lightGray);
		g2.setStroke(stroke);
		g2.setColor(Color.lightGray);
		// g2.drawRoundRect(cp_x_root + 5, cp_y_root + 5, cp_width - 10,
		// cp_height - 10, 15, 15);
		g2.drawRoundRect(cp_x_root + 5, cp_y_root + 5, cp_width - 10, cp_height - 10, 15, 15);
		g2.setFont(logofont);
		g2.setColor(Color.orange);
		g2.drawString("Virgo", cp_x_root + 15, cp_y_root + 40);
		g2.setFont(descfont);
		g2.setColor(Color.white);
		g2.drawString("Meteor Sky View", cp_x_root + 15, cp_y_root + 60);
		g2.setFont(datefont);
		g2.setColor(Color.white);
		g2.drawString(dateonstart, cp_x_root + 15, cp_y_root + 100);
		g2.setFont(timefont);
		g2.drawString(DateTimeUTC.getTimeUTC() + " UTC", cp_x_root + 15, cp_y_root + 120);
		// g2.drawString(myCall, cp_x_root + 15, cp_y_root + 150);
		g2.setFont(textfont);
		g2.drawString("Data for location:", cp_x_root + 15, cp_y_root + 150);
		g2.setFont(timefont);
		g2.drawString(myLocator, cp_x_root + 15, cp_y_root + 170);
		g2.setFont(textfont);
		// g2.drawString("Your call sign:", cp_x_root + 15, sd_y_root +
		// f_sd_axis / 2 + 93);
		// g2.drawString("E", sd_x_root - 20, sd_y_root + f_sd_axis/2 + 7);
		// g2.drawString("Your locator:", cp_x_root + 15, sd_y_root + f_sd_axis
		// / 2 + 143);
		g2.drawString("Enter your locator:", cp_x_root + 15, sd_y_root + f_sd_axis / 2);
		// g2.drawString("Save data", cp_x_root + 40, sd_y_root + f_sd_axis / 2
		// + 190);

		g2.setFont(headerfont);
		g2.drawString("Sky display", cp_x_root + 15, os_y_root - 120);
		g2.setFont(textfont);
		g2.drawString("W - E", cp_x_root + 40, os_y_root - 98);
		g2.drawString("Ticks", cp_x_root + 40, os_y_root - 78);
		g2.drawString("Labels", cp_x_root + 40, os_y_root - 58);
		g2.setFont(headerfont);
		g2.drawString("Azimuth indicator", cp_x_root + 15, os_y_root - 20);
		g2.setFont(textfont);
		g2.drawString("Antenna", cp_x_root + 40, os_y_root + 2);
		g2.drawString("Meteor trails", cp_x_root + 40, os_y_root + 22);
		g2.drawString("Only major", cp_x_root + 40, os_y_root + 42);
		g2.drawString("showers", cp_x_root + 40, os_y_root + 57);
		g2.setFont(headerfont);
		/*
		 * g2.drawString("OSWIN update", cp_x_root + 15, os_y_root + 230);
		 * g2.setFont(textfont); g2.drawString("10 min", cp_x_root + 40,
		 * os_y_root + 252); g2.drawString("1 hour", cp_x_root + 40, os_y_root +
		 * 272); g2.drawString("None", cp_x_root + 40, os_y_root + 292); //--
		 * TMSP g2.
		 * drawString("Click to download  *The Meteorscatter Predictor*  by DL5BAC (DOS .exe file)"
		 * , os_x_root + 80, os_y_root+308);
		 */
		// --------------Sky
		// display---------------------------------------------
		g2.setStroke(stroke); // even divisions: 0, 20...
		g2.setColor(Color.lightGray);
		// g2.drawRoundRect(cp_x_root + cp_width - 5, cp_y_root + 5,
		// dimension.width - cp_width - 5, dimension.height - 10, 15, 15);
		g2.drawRoundRect(cp_x_root + cp_width - 5, cp_y_root + 5, dimension.width - cp_width, dimension.height - 10, 15,
				15);
		g2.setFont(sd_dirfont);
		g2.setColor(Color.white);
		g2.drawString("N", sd_x_root + f_sd_axis / 2 - 7, sd_y_root - 7);
		g2.drawString("S", sd_x_root + f_sd_axis / 2 - 5, sd_y_root + f_sd_axis + 22);
		if (we) {
			g2.drawString("W", sd_x_root - 23, sd_y_root + f_sd_axis / 2 + 7);
			g2.drawString("E", sd_x_root + f_sd_axis + 8, sd_y_root + f_sd_axis / 2 + 7);
		} else {
			g2.drawString("E", sd_x_root - 20, sd_y_root + f_sd_axis / 2 + 7);
			g2.drawString("W", sd_x_root + f_sd_axis + 8, sd_y_root + f_sd_axis / 2 + 7);
		}
		g2.setColor(Color.blue);
		g2.fillOval(sd_x_root, sd_y_root, f_sd_axis, f_sd_axis); // diagram
																	// background
		g2.setColor(Color.lightGray);
		g2.setStroke(stroke); // even divisions: 0, 20...
		g2.drawOval(sd_x_root, sd_y_root, f_sd_axis, f_sd_axis);
		g2.drawOval(sd_x_root + (int) (sd_f * 5.5), sd_y_root + (int) (sd_f * 5.5), f_sd_axis - 2 * (int) (sd_f * 5.5),
				f_sd_axis - 2 * (int) (sd_f * 5.5));
		g2.drawOval(sd_x_root + (int) (sd_f * 21.5), sd_y_root + (int) (sd_f * 21.5),
				f_sd_axis - 2 * (int) (sd_f * 21.5), f_sd_axis - 2 * (int) (sd_f * 21.5));
		g2.drawOval(sd_x_root + (int) (sd_f * 45), sd_y_root + (int) (sd_f * 45), f_sd_axis - 2 * (int) (sd_f * 45),
				f_sd_axis - 2 * (int) (sd_f * 45));
		g2.drawOval(sd_x_root + (int) (sd_f * 74.5), sd_y_root + (int) (sd_f * 74.5),
				f_sd_axis - 2 * (int) (sd_f * 74.5), f_sd_axis - 2 * (int) (sd_f * 74.5));
		g2.drawLine(sd_x_root + f_sd_axis / 2, sd_y_root, sd_x_root + f_sd_axis / 2, sd_y_root + f_sd_axis); // vertical
																												// axis
		g2.drawLine(sd_x_root, sd_y_root + f_sd_axis / 2, sd_x_root + f_sd_axis, sd_y_root + f_sd_axis / 2); // horizontal
																												// axis
		g2.setColor(Color.lightGray);
		g2.setStroke(dotted); // odd divisions: 10, 30...
		g2.drawOval(sd_x_root + (int) (sd_f * 1.5), sd_y_root + (int) (sd_f * 1.5), f_sd_axis - 2 * (int) (sd_f * 1.5),
				f_sd_axis - 2 * (int) (sd_f * 1.5));
		g2.drawOval(sd_x_root + (int) (sd_f * 12.5), sd_y_root + (int) (sd_f * 12.5),
				f_sd_axis - 2 * (int) (sd_f * 12.5), f_sd_axis - 2 * (int) (sd_f * 12.5));
		g2.drawOval(sd_x_root + (int) (sd_f * 32.5), sd_y_root + (int) (sd_f * 32.5),
				f_sd_axis - 2 * (int) (sd_f * 32.5), f_sd_axis - 2 * (int) (sd_f * 32.5));
		g2.drawOval(sd_x_root + (int) (sd_f * 59.5), sd_y_root + (int) (sd_f * 59.5),
				f_sd_axis - 2 * (int) (sd_f * 59.5), f_sd_axis - 2 * (int) (sd_f * 59.5));
		g2.setColor(Color.blue);
		g2.fillRect(x_rect, (sd_y_root + (int) (sd_f * 5.5)) - rect_width / 2, rect_width, rect_width);
		g2.fillRect(x_rect, (sd_y_root + (int) (sd_f * 21.5)) - rect_width / 2, rect_width, rect_width);
		g2.fillRect(x_rect, (sd_y_root + (int) (sd_f * 45)) - rect_width / 2, rect_width, rect_width);
		g2.fillRect(x_rect, (sd_y_root + (int) (sd_f * 74.5)) - rect_width / 2, rect_width, rect_width);
		g2.fillRect(x_rect, (sd_y_root + (int) (sd_f * (180 - 5.5))) - rect_width / 2, rect_width, rect_width);
		g2.fillRect(x_rect, (sd_y_root + (int) (sd_f * (180 - 21.5))) - rect_width / 2, rect_width, rect_width);
		g2.fillRect(x_rect, (sd_y_root + (int) (sd_f * (180 - 45))) - rect_width / 2, rect_width, rect_width);
		g2.fillRect(x_rect, (sd_y_root + (int) (sd_f * (180 - 74.5))) - rect_width / 2, rect_width, rect_width);
		g2.setColor(Color.lightGray);
		g2.setFont(sd_degfont);
		g2.drawString("20", x_str, sd_y_root + (int) (sd_f * 5.5) + 5);
		g2.drawString("40", x_str, sd_y_root + (int) (sd_f * 21.5) + 5);
		g2.drawString("60", x_str, sd_y_root + (int) (sd_f * 45) + 5);
		g2.drawString("80", x_str, sd_y_root + (int) (sd_f * 74.5) + 5);
		g2.drawString("20", x_str, sd_y_root + (int) (sd_f * (180 - 5.5)) + 5);
		g2.drawString("40", x_str, sd_y_root + (int) (sd_f * (180 - 21.5)) + 5);
		g2.drawString("60", x_str, sd_y_root + (int) (sd_f * (180 - 45)) + 5);
		g2.drawString("80", x_str, sd_y_root + (int) (sd_f * (180 - 74.5)) + 5);
		g2.setStroke(stroke);
		if (tracking) {
			for (sdScreenCoordinates t : ticks) {
				g2.setColor(t.col_rad);
				g2.fillOval(t.x, t.y, t.dia_rad, t.dia_rad);
			}
		}
		for (sdScreenCoordinates sc : radiants) {
			g2.setColor(sc.col_rad);
			g2.fillOval(sc.x, sc.y, sc.dia_rad, sc.dia_rad);
		}
		if (labels) {
			g2.setFont(sd_reffont);
			for (sdScreenCoordinates sc : radiants) {
				g2.setColor(Color.blue);
				g2.setColor(Color.white);
				g2.drawString(sc.ref, sc.x_ref, sc.y_ref);
			}
		}
		for (sdScreenCoordinates m : maxima) {
			g2.setStroke(p2);
			g2.setColor(Color.red);
			g2.drawOval(m.x, m.y, m.dia_rad, m.dia_rad);
		}

		// --------OSWIN
		// monitor-------------------------------------------------
		/*
		 * g2.setColor(Color.white); g2.setFont(textfont);
		 * g2.drawString("OSWIN Radar", os_x_root + 2, os_y_root + 98);
		 * g2.drawString("JO54vc", os_x_root + 21, os_y_root + 113);
		 * g2.setColor(Color.blue); g2.fillOval(os_x_root, os_y_root, 80, 80);
		 * //diagram background g2.setColor(Color.lightGray);
		 * g2.setStroke(stroke); g2.drawOval(os_x_root, os_y_root, 80, 80);
		 * g2.drawLine(os_x_root + 40, os_y_root, os_x_root + 40, os_y_root +
		 * 80); //vertical axis g2.drawLine(os_x_root, os_y_root + 40, os_x_root
		 * + 80, os_y_root + 40); //horizontal axis
		 * g2.setColor(Color.lightGray); g2.setStroke(stroke);
		 * g2.drawOval(os_x_root + 3, os_y_root + 3, 80 - 6, 80 - 6);
		 * g2.drawOval(os_x_root + 10, os_y_root + 10, 80 - 20, 80 - 20);
		 * g2.drawOval(os_x_root + 20, os_y_root + 20, 80 - 40, 80 - 40);
		 * g2.drawOval(os_x_root + 33, os_y_root + 33, 80 - 66, 80 - 66); for
		 * (sdScreenCoordinates sc : oswin_radiants) { g2.setColor(sc.col_rad);
		 * g2.fillOval(sc.x, sc.y, 6, 6); } g2.drawImage(oswin_monitor,
		 * os_x_root + 97, os_y_root, this); g2.setColor(Color.white);
		 * g2.setFont(textfont); g2.drawString("Meteor", os_x_root + 657,
		 * os_y_root + 18); g2.drawString("events:", os_x_root + 657, os_y_root
		 * + 32); if (oswin_online) { if (oswin_events < 10) { ev_offset = 16; }
		 * else { if (oswin_events < 100) { ev_offset = 11; } else { if
		 * (oswin_events < 1000) { ev_offset = 6; } else { if (oswin_events <
		 * 10000) { ev_offset = 1; } } } } g2.setFont(ai_degfont);
		 * g2.drawString(oswin_events.toString(), os_x_root + 657 + ev_offset,
		 * os_y_root + 53); } else { g2.setFont(textfont);
		 * g2.drawString("no data", os_x_root + 657 + ev_offset, os_y_root +
		 * 53); g2.setFont(ai_degfont); g2.setColor(Color.red);
		 * oswin_msg="OSWIN radar is still out of service...";
		 * g2.drawString(oswin_msg, os_x_root+340, os_y_root-5); }
		 * //g2.setFont(ai_degfont); //g2.
		 * drawString("OSWIN data not reliable at the moment: IAP is doing kind of fast forward"
		 * , os_x_root + 140, os_y_root + 22); //g2.
		 * drawString("Expect data to be synchronized on Dec 14th around 15:00 UTC"
		 * , os_x_root + 140, os_y_root + 36);
		 * 
		 */
		// --------Table---------------------------------------------------------
		g2.setFont(btablefont);
		g2.setColor(Color.white);
		if (showers.isEmpty()) {
			g2.drawString("No meteor showers with ZHR > 1 today", 10, 448);
		} else {
			i = 1;
			g2.setColor(Color.white);
			g2.drawRoundRect(t_x_root, t_y_root - vertspace + 15, namewidth - 5, vertspace, 8, 8);
			g2.drawRoundRect(t_x_root + namewidth - 5, t_y_root - vertspace + 15, 45, vertspace, 8, 8);
			g2.drawRoundRect(t_x_root + namewidth + 47, t_y_root - vertspace + 15, 50, vertspace, 8, 8);
			g2.drawRoundRect(t_x_root + namewidth + 97, t_y_root - vertspace + 15, 50, vertspace, 8, 8);
			g2.drawRoundRect(t_x_root + namewidth + 155, t_y_root - vertspace + 15, 42, vertspace, 8, 8);
			g2.drawRoundRect(t_x_root + namewidth + 197, t_y_root - vertspace + 15, 45, vertspace, 8, 8);
			g2.drawRoundRect(t_x_root + namewidth + 250, t_y_root - vertspace + 15, 40, vertspace, 8, 8);
			g2.drawRoundRect(t_x_root + namewidth + 290, t_y_root - vertspace + 15, 50, vertspace, 8, 8);
			g2.drawRoundRect(t_x_root + namewidth + 340, t_y_root - vertspace + 15, 210, vertspace, 8, 8);
			g2.drawString("Meteor Shower", t_x_root + 5, t_y_root + 10);
			g2.drawString("Ref.", t_x_root + namewidth + 5, t_y_root + 10);
			g2.drawString("Rise", t_x_root + namewidth + 59, t_y_root + 10);
			g2.drawString("Set", t_x_root + namewidth + 112, t_y_root + 10);
			g2.drawString("RA", t_x_root + namewidth + 167, t_y_root + 10);
			strwidth = fontmetrics.stringWidth(String.valueOf("Decl."));
			g2.drawString("Decl.", t_x_root + namewidth + 235 - strwidth, t_y_root + 10);
			strwidth = fontmetrics.stringWidth(String.valueOf("ZHR"));
			g2.drawString("ZHR", t_x_root + namewidth + 282 - strwidth, t_y_root + 10);
			g2.drawString("Class", t_x_root + namewidth + 297, t_y_root + 10);
			g2.drawString("Visual Activity Peak", t_x_root + namewidth + 350, t_y_root + 10);
			g2.setFont(tablefont);
			for (Shower s : showers) {
				g2.setColor(Color.lightGray);
				g2.drawRoundRect(t_x_root, t_y_root + 14 - vertspace + i * vertspace + 5, namewidth - 5, vertspace, 8,
						8);
				g2.drawRoundRect(t_x_root + namewidth - 5, t_y_root + i * vertspace, 45, vertspace, 8, 8);
				if (s.set.equals("")) {
					g2.drawRoundRect(t_x_root + namewidth + 47, t_y_root + i * vertspace, 100, vertspace, 8, 8);
				} else {
					g2.drawRoundRect(t_x_root + namewidth + 47, t_y_root + i * vertspace, 50, vertspace, 8, 8);
					g2.drawRoundRect(t_x_root + namewidth + 97, t_y_root + i * vertspace, 50, vertspace, 8, 8);
				}
				g2.drawRoundRect(t_x_root + namewidth + 155, t_y_root + i * vertspace, 42, vertspace, 8, 8);
				g2.drawRoundRect(t_x_root + namewidth + 197, t_y_root + i * vertspace, 45, vertspace, 8, 8);
				g2.drawRoundRect(t_x_root + namewidth + 250, t_y_root + i * vertspace, 40, vertspace, 8, 8);
				g2.drawRoundRect(t_x_root + namewidth + 290, t_y_root + i * vertspace, 50, vertspace, 8, 8);
				g2.drawRoundRect(t_x_root + namewidth + 340, t_y_root + i * vertspace, 210, vertspace, 8, 8);
				g2.setColor(Color.white);
				g2.drawString(s.name, t_x_root + 5, t_y_root + 14 + i * vertspace);
				g2.drawString(s.ref, t_x_root + namewidth + 5, t_y_root + 14 + i * vertspace);
				g2.drawString(s.rise, t_x_root + namewidth + 55, t_y_root + 14 + i * vertspace);
				g2.drawString(s.set, t_x_root + namewidth + 105, t_y_root + 14 + i * vertspace);
				strwidth = fontmetrics.stringWidth(String.valueOf(Math.round(s.ra)));
				g2.drawString(String.valueOf(Math.round(s.ra)), t_x_root + namewidth + 189 - strwidth,
						t_y_root + 14 + i * vertspace);
				strwidth = fontmetrics.stringWidth(String.valueOf(Math.round(s.decl)));
				g2.drawString(String.valueOf(Math.round(s.decl)), t_x_root + namewidth + 235 - strwidth,
						t_y_root + 14 + i * vertspace);
				strwidth = fontmetrics.stringWidth(String.valueOf(s.zhr));
				g2.drawString(s.zhr, t_x_root + namewidth + 285 - strwidth, t_y_root + 14 + i * vertspace);
				g2.drawString(String.valueOf(s.mclass), t_x_root + namewidth + 308, t_y_root + 14 + i * vertspace);
				for (sdScreenCoordinates sc : radiants) {
					if (sc.ref.equals(s.ref)) {
						g2.setColor(sc.col_rad);
						g2.fillOval(t_x_root + namewidth + 322, t_y_root + 14 - sc.dia_rad + i * vertspace, sc.dia_rad,
								sc.dia_rad);
					}
				}
				if (s.S_ID != 136) {
					g2.setColor(Color.white);
					g2.drawString(String.valueOf(s.peak_date), t_x_root + namewidth + 350,
							t_y_root + 14 + i * vertspace);
					g2.drawString(String.valueOf(s.peak_time), t_x_root + namewidth + 440,
							t_y_root + 14 + i * vertspace);
					g2.setColor(Color.gray);
					for (int j = 0; j <= 3; j++) {
						g2.fillRect(t_x_root + namewidth + 490 + j * 5, (t_y_root + 12 + i * vertspace) - j * 2, 3,
								3 + j * 2);
					}
					for (int j = 3; j >= 0; j--) {
						g2.fillRect(t_x_root + namewidth + 530 - j * 5, (t_y_root + 12 + i * vertspace) - j * 2, 3,
								3 + j * 2);
					}
					g2.fillRect(t_x_root + namewidth + 510, (t_y_root + 12 + i * vertspace) - 8, 3, 11);
					g2.setColor(Color.cyan);
					if (s.peak_date.equals(dateonstart)) {
						g2.setColor(Color.orange);
						g2.fillRect(t_x_root + namewidth + 510, (t_y_root + 12 + i * vertspace) - 8, 3, 11);
					} else {
						if (s.diff_peak <= -76) {
							g2.fillRect(t_x_root + namewidth + 490, (t_y_root + 12 + i * vertspace), 3, 3);
						} else {
							if (s.diff_peak <= -51) {
								g2.fillRect(t_x_root + namewidth + 495, (t_y_root + 12 + i * vertspace) - 2, 3, 5);
							} else {
								if (s.diff_peak <= -26) {
									g2.fillRect(t_x_root + namewidth + 500, (t_y_root + 12 + i * vertspace) - 4, 3, 7);
								} else {
									if (s.diff_peak < 0) {
										g2.fillRect(t_x_root + namewidth + 505, (t_y_root + 12 + i * vertspace) - 6, 3,
												9);
									} else {
										if (s.diff_peak <= 25) {

											g2.fillRect(t_x_root + namewidth + 515, (t_y_root + 12 + i * vertspace) - 6,
													3, 9);
										} else {
											if (s.diff_peak <= 50) {
												g2.fillRect(t_x_root + namewidth + 520,
														(t_y_root + 12 + i * vertspace) - 4, 3, 7);

											} else {
												if (s.diff_peak <= 75) {
													g2.fillRect(t_x_root + namewidth + 525,
															(t_y_root + 12 + i * vertspace) - 2, 3, 5);
												} else {
													if (s.diff_peak <= 100) {
														g2.fillRect(t_x_root + namewidth + 530,
																(t_y_root + 12 + i * vertspace), 3, 3);
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				i++;
			}
		}

		// ----------------Azimuth
		// indicator-------------------------------------
		g2.setColor(Color.lightGray);
		g2.setStroke(stroke);
		g2.drawOval(ai_x_root, ai_y_root, f_ai_axis, f_ai_axis);
		g2.setFont(ai_degfont);
		g2.setColor(Color.white);
		g2.drawString("0", ai_x_root + f_ai_axis / 2 - 4, ai_y_root + 25);
		g2.drawString("30", ai_x_root + f_ai_axis / 3 * 2 + 4, ai_y_root + f_ai_axis / 6 - 3);
		g2.drawString("60", ai_x_root + Math.round(f_ai_axis / 3 * 2.5) + 6, ai_y_root + f_ai_axis / 3 - 5);
		g2.drawString("90", ai_x_root + f_ai_axis - 25, ai_y_root + f_ai_axis / 2 + 5);
		g2.drawString("120", ai_x_root + Math.round(f_ai_axis / 3 * 2.5) + 1, ai_y_root + f_ai_axis / 3 * 2 + 13);
		g2.drawString("150", ai_x_root + f_ai_axis / 3 * 2 + 4, ai_y_root + f_ai_axis / 6 * 5 + 13);
		g2.drawString("180", ai_x_root + f_ai_axis / 2 - 11, ai_y_root + f_ai_axis - 14);
		g2.drawString("210", ai_x_root + f_ai_axis / 5 + 11, ai_y_root + f_ai_axis / 6 * 5 + 13);
		g2.drawString("240", ai_x_root + f_ai_axis / 8 - 14, ai_y_root + f_ai_axis / 3 * 2 + 13);
		g2.drawString("270", ai_x_root + 5, ai_y_root + f_ai_axis / 2 + 5);
		g2.drawString("300", ai_x_root + f_ai_axis / 8 - 14, ai_y_root + f_ai_axis / 3 - 5);
		g2.drawString("330", ai_x_root + f_ai_axis / 5 + 9, ai_y_root + f_ai_axis / 6 - 3);

		// ticks, 10 degrees spaced
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		for (XY xy : compasscard_10) {
			g2.drawLine(ai_x_root + f_ai_axis / 2, ai_y_root + f_ai_axis / 2, xy.x + ai_x_root + f_ai_axis / 6 - 10,
					xy.y + ai_y_root + f_ai_axis / 6 - 10);
		}

		// ticks, 30 degrees spaced
		g2.setColor(Color.cyan);
		g2.setStroke(p3);
		for (XY xy : compasscard_30) {
			g2.drawLine(ai_x_root + f_ai_axis / 2, ai_y_root + f_ai_axis / 2, xy.x + ai_x_root + f_ai_axis / 6 - 10,
					xy.y + ai_y_root + f_ai_axis / 6 - 10);
		}

		// ticks, 5 degrees spaced
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		for (XY xy : compasscard_5) {
			g2.drawRect(xy.x + ai_x_root + f_ai_axis / 6 - 5, xy.y + ai_y_root + f_ai_axis / 6 - 5, 1, 1);
		}
		g2.setColor(Color.black);
		g2.fillOval(ai_x_root + f_ai_axis / 4 - 20, ai_y_root + f_ai_axis / 4 - 20, f_ai_axis / 2 + 40,
				f_ai_axis / 2 + 40);

		g2.setColor(Color.lightGray);

		// vertical axis
		g2.drawLine(ai_x_root + f_ai_axis / 2, ai_y_root + f_ai_axis / 4, ai_x_root + f_ai_axis / 2,
				ai_y_root + f_ai_axis / 4 * 3);

		// horizontal axis
		g2.drawLine(ai_x_root + f_ai_axis / 4 + 4, ai_y_root + f_ai_axis / 2, ai_x_root + f_ai_axis / 4 * 3,
				ai_y_root + f_ai_axis / 2);
		g2.setStroke(stroke);
		g2.setColor(Color.black);
		g2.fillOval(ai_x_root + f_ai_axis / 2 - 14, ai_y_root + f_ai_axis / 2 - 14, 28, 28);

		g2.setColor(Color.white);
		g2.setFont(ai_dirfont);
		g2.drawString("N", ai_x_root + f_ai_axis / 2 - 4, ai_y_root + f_ai_axis / 4 - 4);
		g2.drawString("E", ai_x_root + f_ai_axis / 4 * 3 + 7, ai_y_root + f_ai_axis / 2 + 5);
		g2.drawString("S", ai_x_root + f_ai_axis / 2 - 3, ai_y_root + f_ai_axis / 5 * 4);
		g2.drawString("W", ai_x_root + f_ai_axis / 5, ai_y_root + f_ai_axis / 2 + 5);

		for (AzimuthIndicator.AiScreenCoordinates s : qtf) {
			g2.setColor(s.col_rad);
			g2.setStroke(aineedle);
			g2.drawLine(s.x1, s.y1, s.x2, s.y2);
		}
		g2.setStroke(stroke);
		g2.setColor(Color.darkGray);
		g2.fillOval(ai_x_root + f_ai_axis / 2 - 8, ai_y_root + f_ai_axis / 2 - 8, 16, 16);
		g2.setColor(Color.lightGray);
		g2.setStroke(p3);
		g2.drawOval(ai_x_root + f_ai_axis / 2 - 8, ai_y_root + f_ai_axis / 2 - 8, 16, 16);
	}
}

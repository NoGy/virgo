package afu;

public class Conversion {

    /** degrees per hour (hour angle) */
    private static final int deg_hour = 15;
    /** radians per hour (hour angle) */
    private static final double rad_hour = Math.toRadians(deg_hour);
    /** conversion factor between sidereal time and UT */
    private static final double t_conv = 0.9972696;

    public static double getFrac(double x) {
        return (x - Math.floor(x));
    }
    
    public static double getFracMod(double x, double y) {
        return (y * ((x / y) - Math.floor(x / y)));
    }
    
    public static double roundToDecimal(double x, double y) {
        return Math.round(x * y * 10.0) / (y * 10.0);
    }
    
    public static String toTime(double dectime) {
        String hour = "";
        if (dectime < 10) { hour = "0"; }
        hour = hour + (int)dectime;
        dectime = (dectime - (int)dectime) * 60.0;
        String min = "";
        if (dectime < 10) { min = "0"; }
        min = min + (int)dectime;
        dectime = (dectime - (int)dectime) * 60.0;
        String sec = "";
        if (dectime < 10) { sec = "0"; }
        sec = sec + (int)dectime;
        return (hour + ":" + min + ":" + sec);
    }
    
    public static String toShortTime(String time) {
        String[] d = time.split(":");
        return d[0] + ":" + d[1];
    }

    public static String toShortTime(double dectime) {
        String hour = "";
        if (dectime < 10) { hour = "0"; }
        hour = hour + (int)dectime;
        dectime = (dectime - (int)dectime) * 60.0;
        String min = "";
        if (dectime < 10) { min = "0"; }
        min = min + (int)dectime;
        dectime = (dectime - (int)dectime) * 60.0;
        return (hour + ":" + min);
    }

    public static String toSolarTime(double radtime) {
        return toTime(radtime / rad_hour * t_conv);
    }
    
    public static String toShortSolarTime(double radtime) {
        return toShortTime(radtime / rad_hour * t_conv);
    }
    

    public static double toNavAzimuth(double azimuth) {
        // nautical azimuth [rad] N 0°, E 90°, S 180°, W 270°
        return getFracMod((azimuth + Math.PI), Math.PI * 2);
    }
}
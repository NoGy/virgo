package afu;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

//-- Database interface -----------------------------------------------------
    public class DBclient {
// TEST Server      
   
        private String url = "https://dl1dbc.net/Meteorscatter/dbServer.pl";
    //    private String url = "http://dl1dbc.net/Meteorscatter/dbServer.pl";
    //    private String url = "http://localhost/ms/DBserver/dbServer.pl";
        private URL dbServer; 
        private URLConnection dbConnection;
        private BufferedReader in;
        private String inputLine;
        private PrintWriter out;
        private String query;
        private ArrayList<String> dbRetrieval;

        public DBclient() {
        }

        public ArrayList<String> retrieveData(String sql) {
            query = "query=" + sql;
            try {
                dbServer = new URL(url);
                dbConnection = dbServer.openConnection();
            } catch (MalformedURLException e) {
                System.out.println(e);            
                
            } catch (IOException e) { 
                System.out.println(e);            
            }
            dbConnection.setDoOutput(true); 
            try {
                out = new PrintWriter(dbConnection.getOutputStream());
                out.println(query);
                out.close();

                in = new BufferedReader(new InputStreamReader(dbConnection.getInputStream()));
                dbRetrieval = new ArrayList<String>();
                while ((inputLine = in.readLine()) != null) {
                    dbRetrieval.add(inputLine);
                }
                in.close();
            } catch (IOException e) { 
                System.out.println(e);
            }
            return dbRetrieval;
        }
    }